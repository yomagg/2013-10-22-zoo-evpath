# @file readme.txt
# @date Created on Dec 13, 2013
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

DESCRIPTION
===========

Creating and removing stones from the splitter stone. 

The sender creates the splitter stone and adds as targets output stones accordingly
to provided contact info to receivers. After the specified period of time
it starts removing periodically the split targets.

The receiver just consumes the event send by the sender. The receivers should
observed no incoming events after elapsing the time specified for the sender
one by one. Eventually all splitter targets should be removed and destroyed.


BUILD
=====

# change paths to EVPath
$ vi SConstruct
...

# build
$ scons

# clean 
$ scons -c

There is a Makefile included for your convenience.

RUNNING
=======
First run receivers, then run the sender with receivers' contact info as
input parameters to the sender. You need to provide the parameter how
long each program will run. Run programs without parameters to see what 
the available options are.

$ ./receiver 300
$ ./receiver 300
$ ./sender  20 0:AAIAAJTJ8o2UZQAAATkCmDo8PYA= 0:AAIAAJTJ8o2dZQAAATkCmDo8PYA=


TROUBLESHOOTING
================
2013-12-13 - the destroying of output stones in the sender doesn't work,
             it causes that the sender hangs up.
             
             Solution: I commented this part of code.

NOTES
=====
2013-12-13 - sent to Greg
# EOF
