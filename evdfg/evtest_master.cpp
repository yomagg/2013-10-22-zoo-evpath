/**
 *  @file   evtest_master.cpp
 *
 *  @date   Created on: Dec 16, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

// the pointer to the dfg structure
static EVdfg dfg = nullptr;

static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";

	// the 0 argument is from ev_dfg.c -- STATUS_SUCCESS; this
	// influence the dfg->nodes[xxx].shutdown_status_contribution
	EVdfg_shutdown(dfg, 0);

	// TODO what exactly should this handler return? 0 (original test) or 1
	// (see e.g. the evpath test)
    return 0;
}

int main(int argc, char **argv){

	int status = 0;;
	CManager cm = CManager_create();
	CMlisten(cm);

	// local DFG support
	// what does it mean -1 for the parameter for the stone (EVstone)
	//EVsource source_handle = EVcreate_submit_handle(cm, -1, complex_format_list);
	//EVdfg_register_source(const_cast<char*>(MASTER_SOURCE_NAME.c_str()), source_handle);
	EVdfg_register_sink_handler(cm, const_cast<char*>(HANDLER_NAME.c_str()), complex_format_list,
			(EVSimpleHandlerFunc) complex_handler);

	// DFG creation
	dfg = EVdfg_create(cm);
	// I guess this is the master dfg contact list
	char *str_contact = EVdfg_get_contact_list(dfg);

	// the name of nodes that the master expect to join the dfg
	char *nodes[] = {"a", "b", nullptr};
	EVdfg_register_node_list(dfg, nodes);

	EVdfg_stone src_id = EVdfg_create_source_stone(dfg, const_cast<char*>( MASTER_SOURCE_NAME.c_str()));
	EVdfg_assign_node(src_id, "b");

	EVdfg_stone sink_id = EVdfg_create_sink_stone(dfg, const_cast<char*>(HANDLER_NAME.c_str()));
	EVdfg_assign_node(sink_id, "a");

	// connect source with sink
	EVdfg_link_port(src_id, 0, sink_id);

	// deploy EVdfg
	EVdfg_realize(dfg);

	// I  guess we want to be a sink
	EVdfg_join_dfg(dfg, nodes[0], str_contact);

	cout << "Dfg joined: " << nodes[0] << "\n";
	cout << "Dfg waiting for node " << nodes[1] << " to join\n";

	cout << "Run ./evtest_worker " << nodes[1] << " " << str_contact << "\n";
	cout << "or\n";
	cout << "CMSelfFormats=1 ./evtest_worker " << nodes[1] << " " << str_contact << "\n";
	free(str_contact);
	str_contact = nullptr;

	// wait for the appearance of the source
	// seems as it always returns 1, anyway
	EVdfg_ready_wait(dfg);

/*	if (EVdfg_source_active(source_handle)){
		struct complex_rec rec = { 23.04, 12.01 };
		EVsubmit(source_handle, &rec, nullptr);
	} */

	if (0 == EVdfg_active_sink_count(dfg)){
		EVdfg_ready_for_shutdown(dfg);
	}

	status = EVdfg_wait_for_shutdown(dfg);

	//EVfree_source(source_handle);
	CManager_close(cm);

	return status;
}
