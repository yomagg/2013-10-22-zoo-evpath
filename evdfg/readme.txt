# @file readme.txt
# @date Created on Feb 16, 2014
# @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
#

BUILD
=====

> scons

RUNNING
=======
First run the master, and master will print the
instruction how to run a worker

> ./evtest_master 

# EOF
