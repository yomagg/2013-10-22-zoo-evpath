/**
 *  @file   evtest_worker.cpp
 *
 *  @date   Created on: Dec 16, 2013
 *  @author Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */
#include <iostream>

#include "evpath.h"
#include "ev_dfg.h"

#include "data.h"

using namespace std;

EVdfg dfg = nullptr;

/*static int
complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
	struct complex_rec * event = static_cast<complex_rec*> (vevent);
	cout << "Receiver got: (" << event->r << ", " << event->i << ")\n";

	// the 0 argument is from ev_dfg.c -- STATUS_SUCCESS; this
	// influence the dfg->nodes[xxx].shutdown_status_contribution
	EVdfg_shutdown(dfg, 0);

	// TODO what exactly should this handler return? 0 (original test) or 1
	// (see e.g. the evpath test)
    return 0;
}*/


int main(int argc, char *argv[]) {

	CManager cm = CManager_create();

	if (argc != 3) {
		cout << "USAGE:\n";
		cout << argv[0] << " nodename mastercontact\n";
		cout << "EX. " << argv[0] << " b AAIAAJTJ8o3CZQAAATkCmEoBqMA=\n";

		return 0;
	}

	// create dfg
	dfg = EVdfg_create(cm);

	// TODO not sure what -1 means (-1 indicates a stone id to which the data
	// will be submitted
	EVsource src_id = EVcreate_submit_handle(cm, -1, complex_format_list);

	EVdfg_register_source(const_cast<char*>(MASTER_SOURCE_NAME.c_str()), src_id);
//	EVdfg_register_sink_handler(cm, const_cast<char*>(MASTER_SOURCE_NAME.c_str()), complex_format_list,
//			(EVSimpleHandlerFunc) complex_handler);

	EVdfg_join_dfg(dfg, argv[1], argv[2]);
	cout << argv[1] << " joining master\n";

	EVdfg_ready_wait (dfg);

	if (EVdfg_active_sink_count(dfg) == 0) {
		EVdfg_ready_for_shutdown(dfg);
	}

	if (EVdfg_source_active(src_id)) {
		struct complex_rec rec = {15.1, 16.3};
		cout << "Submitting (" << rec.r << "," << rec.i << ")\n";

		// submit would be quietly ignored if source is not active
		EVsubmit(src_id, &rec, nullptr);
	}
	EVfree_source(src_id);

	return EVdfg_wait_for_shutdown(dfg);
}
