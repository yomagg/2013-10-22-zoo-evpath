/**
 * extract_local.cpp
 *
 *  Created on: Dec 9, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */


#include <unistd.h>
#include <sys/types.h>

#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

#include "evpath.h"
#include "revpath.h"

#ifdef __cplusplus
}
#endif

#include "triv.h"
using namespace std;

// this convention is to get line numbers if something is wrong in COD
// '\n\'
char *ECL_generate = "{\n\
    static int count = 0;\n\
    long sum = 0;\n\
    output.integer_field = (int) lrand48() % 100;\n\
    sum = sum + output.integer_field % 100;\n\
    output.short_field = ((short) lrand48());\n\
    sum = sum + output.short_field % 100;\n\
    output.long_field = ((long) lrand48());\n\
    sum = sum + output.long_field % 100;\n\
\n\
    output.nested_field.item.r = drand48();\n\
    sum = sum + ((int) (output.nested_field.item.r * 100.0)) % 100;\n\
    output.nested_field.item.i = drand48();\n\
    sum = sum + ((int) (output.nested_field.item.i * 100.0)) % 100;\n\
\n\
    output.double_field = drand48();\n\
    sum = sum + ((int) (output.double_field * 100.0)) % 100;\n\
    output.char_field = lrand48() % 128;\n\
    sum = sum + output.char_field;\n\
    sum = sum % 100;\n\
    output.scan_sum = (int) sum;\n\
    count++;\n\
    return count == 1;\n\
}";

char my_code[]="{\n\
		output.r = 13.0;\n\
		output.i = 13.13;\n\
		\n\
		return 1;\n\
	}";

int message_counts[3];

struct client_data rec0, rec1, rec2;


static int complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec *event = static_cast<struct complex_rec *> (vevent);
	struct client_data *cdata = static_cast<struct client_data *> (client_data);

	int output_index = cdata->output_index;

	std::cout << "Complex=( " << event->r << "," << event->i << " )\n";

	if (nullptr != cdata->message_count ){
		int tmp = cdata->message_count[output_index];
		cdata->message_count[output_index] = tmp + 1;
	}

	return 0;
}

static void alive_handler(CManager cm, CMConnection conn, void *alive_v,
		void *client_data, attr_list attrs){
	EVaction action, faction;
	EVstone term1, term2, bridge0, bridge1, bridge2, router_stone, source_stone;
	char *filter = nullptr, *gen_spec = nullptr;

	message_counts[0] = message_counts[1] = message_counts[2] = 0;

	// create local stones
	EVstone term0 = EValloc_stone(cm);
	rec0.output_index = 0;
	rec0.message_count = &message_counts[0];
	EVassoc_terminal_action(cm, term0, complex_format_list, complex_handler, (void*) &rec0);

	attr_list self_list = CMget_contact_list(cm);
	char * self_list_str = attr_list_to_string(self_list);
	cout << "My contact list: " << self_list_str << "\n";
	free(self_list_str);

}

int main(int argc, char **argv){

	CManager cm;
	attr_list contact_list = nullptr;
	char *string_list = nullptr;

	cm = CManager_create();
	CMfork_comm_thread(cm);
	cout << "Forked a communication thread\n";

	CMlisten(cm);
	contact_list = CMget_contact_list(cm);

	if (contact_list) {
		string_list = attr_list_to_string(contact_list);
		free_attr_list(contact_list);
	} else {
		cerr << "ERROR: Issues with getting the contact list. Quitting ...\n";
		return 1;
	}


	// get the contact information from the remote
	CMFormat alive_format;

	// register a message format you expect to receive and a handler
	// to serve this format
	alive_format = CMregister_format(cm, alive_format_list);
	CMregister_handler(alive_format, alive_handler, nullptr);

	cout << "Run remote with: " << string_list << "\n";

	// give the process time to start, run and send the data
	for( int i = 0; i < 20; ++i){
		if ((message_counts[0] == 5)
			//&& (message_counts[1] == 0)
			//&& (message_counts[2] == 5)
			)
			break;
		CMsleep(cm, 1);
	}

	CMsleep(cm, 120);

	free(string_list);
	CManager_close(cm);

	cout << "Message count[0]=" << message_counts[0] << " (5)\n"
	    		<< "Message count[1]=" << message_counts[1] << " (0)\n"
	    		<< "message count[2]=" << message_counts[2] << " (5)\n";

	return 0;
}
