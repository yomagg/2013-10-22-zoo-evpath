/**
 * term_local.cpp
 *
 *  Created on: Dec 9, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

#include <unistd.h>
#include <sys/types.h>
#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

#include "evpath.h"
#include "revpath.h"

#ifdef __cplusplus
}
#endif

#include "triv.h"

using namespace std;


// how many messages I want to receive
static int message_count = 0;

// seems that it should return 1 (not zero)
// 0 - discard
// 1 - keep
char my_code[] = "{\n\
		output.r = 11.1;\n\
		output.i = 12.2;\n\
\n\
		return 1;\n\
}";


static int complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec * event = static_cast<struct complex_rec *>(vevent);

	cout << "complex_handler received: (" << event->r << "," << event->i << ")\n";

//	if(attrs){
//		dump_attr_list(attrs);
//	}

	message_count++;

	cout << "complex_handler message_count=" << message_count << "\n";

	return 0;
}

static void alive_handler(CManager cm, CMConnection conn, void *alive_v,
		void *client_data, attr_list attrs){

	// specify the action and data format on which the action will be performed
	char *action_spec = create_transform_action_spec(nullptr, complex_format_list,my_code);

	// allocate the remote stones
	EVstone rstone = REValloc_stone(conn);
	EVaction action = REVassoc_immediate_action( conn, rstone, action_spec);
	EVstone routput_stone = REValloc_stone(conn);
	REVaction_set_output(conn, rstone, action, 0, routput_stone);

	// now allocate a terminal local stone and say what data and what operation
	// will be performed on that data
	EVstone local_stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, local_stone, complex_format_list, complex_handler, &message_count);

	// connect routput_stone with the local_stone
	attr_list tmp_list = CMget_contact_list(cm);
	REVassoc_bridge_action(conn, routput_stone, tmp_list, local_stone);
	free_attr_list(tmp_list);
	free(action_spec);

	// this will submit an event on rstone every 0 sec and 500 usec
	REVenable_auto_stone(conn, rstone, 0, 100);
}

int main(int argc, char **argv){
	CManager cm;

	cm = CManager_create();
	CMfork_comm_thread(cm);
//	cout << "Forked a communication thread\n";

	CMlisten(cm);

	// register the format and a handler of data that I will receive
	CMFormat alive_format = CMregister_format(cm, alive_format_list);
	CMregister_handler(alive_format, alive_handler, nullptr);

	// get a contact list
	attr_list contact_list = nullptr;
	contact_list =  CMget_contact_list(cm);

	char * string_list = nullptr;
	if (contact_list) {
		string_list = attr_list_to_string(contact_list);
		free_attr_list(contact_list);
	} else {
		cerr << "ERROR: Issues with getting the contact list. Quitting ...\n";
		return 1;
	}

	cout << "Run remote with: " << string_list << "\n";

	// delay

	for( int i = 0; i < 30; ++i){
		if (message_count == 1)
			break;
		CMsleep(cm, 1);
	}
	CMsleep(cm, 30);

	free(string_list);
	//CMrun_network(cm);
	CManager_close(cm);
	cout << "Message count = " << message_count << "\n";

	return !(0 != message_count );
}
