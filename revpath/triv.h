/**
 * triv.h
 *
 *  Created on: Dec 6, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */
#ifndef TRIV_H_
#define TRIV_H_

// ------------------------------------------
// complex
// -------------------------------------------
struct complex_rec {
    double r;
    double i;
};

static FMField complex_field_list[] = {
    {"r", "double", sizeof(double), FMOffset(struct complex_rec*, r)},
    {"i", "double", sizeof(double), FMOffset(struct complex_rec*, i)},
    {NULL, NULL, 0, 0}
};

static FMStructDescRec complex_format_list[] = {
		{"complex_rec", complex_field_list, sizeof(struct complex_rec), nullptr},
		{ nullptr, nullptr, 0, nullptr }
};

// ------------------------------------------
// alive message
// -----------------------------------------
struct alive_msg {
	char *contact;
};

FMField alive_field_list[] = {
  {"contact", "string", sizeof(char*), FMOffset(struct alive_msg *, contact)},
  {nullptr, nullptr, 0, 0}
};

FMStructDescRec alive_format_list[] = {
  { "alive", alive_field_list, sizeof(struct alive_msg), nullptr },
  { nullptr, nullptr, 0, nullptr}
};

// ----------------------------
// other
// -----------------------------
struct client_data {
    int output_index;
    int *message_count;
};

#endif /* TRIV_H_ */
