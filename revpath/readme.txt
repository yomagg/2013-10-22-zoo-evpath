# readme.txt
# Created on: Dec 4, 2013
# Author: Magda S. aka Magic Magg magg dot gatech at gmail.com
#

These examples are from evpath/source/rtests

Just to learn how to use the remote evpath.


RUNNING EXAMPLES
=================
* implicit forking (the regression test mode) and verbose mode

CMSelfFormats=1 ./remote_terminal_test -v

* no forking; for machines connected to internet skip CMSelfFormats

CMSelfFormats=1 ./remote_terminal_test -n
CMSelfFormats=1 ./remote_terminal_test -s AAIAAJTJ8o24ZQAAATkCmAZh148=

NOTES
=======

* 2013-12-10 

  - Only the term_local, term_remote example are completed.
  it creates remotely the terminal stone.
  
  - extract_local - is not finished; extract_remote should be finished; extract_test.cpp,
  mextract_test.cpp should work.
  
  - evtest is exemplified by triv_local and triv_remote (sort of works)
  
# EOF 
