//#include "config.h"

#ifdef __cplusplus

#include <unistd.h>
#include <sys/types.h>

extern "C" {
#endif

#include "evpath.h"
#include "revpath.h"

#ifdef __cplusplus
}
#endif

//#include "triv.h"
using namespace std;
#include <iostream>

#include <stdio.h>
//#include <atl.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <arpa/inet.h>
#ifdef HAVE_WINDOWS_H
#include <windows.h>
#define drand48() (((double)rand())/((double)RAND_MAX))
#define lrand48() rand()
#define srand48(x)
#else
#include <sys/wait.h>
#endif

typedef struct _complex_rec {
    double r;
    double i;
} complex, *complex_ptr;

typedef struct _nested_rec {
    complex item;
} nested, *nested_ptr;

static FMField nested_field_list[] =
{
    {"item", "complex", sizeof(complex), FMOffset(nested_ptr, item)},
    {NULL, NULL, 0, 0}
};

static FMField complex_field_list[] =
{
    {"r", "double", sizeof(double), FMOffset(complex_ptr, r)},
    {"i", "double", sizeof(double), FMOffset(complex_ptr, i)},
    {NULL, NULL, 0, 0}
};

typedef struct _simple_rec {
    int integer_field;
    short short_field;
    long long_field;
    nested nested_field;
    double double_field;
    char char_field;
    int scan_sum;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {"short_field", "integer",
     sizeof(short), FMOffset(simple_rec_ptr, short_field)},
    {"long_field", "integer",
     sizeof(long), FMOffset(simple_rec_ptr, long_field)},
    {"nested_field", "nested",
     sizeof(nested), FMOffset(simple_rec_ptr, nested_field)},
    {"double_field", "float",
     sizeof(double), FMOffset(simple_rec_ptr, double_field)},
    {"char_field", "char",
     sizeof(char), FMOffset(simple_rec_ptr, char_field)},
    {"scan_sum", "integer",
     sizeof(int), FMOffset(simple_rec_ptr, scan_sum)},
    {NULL, NULL, 0, 0}
};

static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {"complex", complex_field_list, sizeof(complex), NULL},
    {"nested", nested_field_list, sizeof(nested), NULL},
    {NULL, NULL, 0, NULL}
};

static FMStructDescRec complex_format_list[] = {
		{"complex", complex_field_list, sizeof(complex), nullptr},
		{ nullptr, nullptr, 0, nullptr }
};

int quiet = 1;

static int message_count = 0;

static int complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	complex_ptr event = static_cast<complex_ptr>(vevent);

	cout << "complex_handler received: (" << event->r << "," << event->i << ")\n";

//	if(attrs){
//		dump_attr_list(attrs);
//	}

	message_count ++;

	cout << "complex_handler message_count=" << message_count << "\n";

	return 0;
}

static
int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_rec_ptr event = static_cast<simple_rec_ptr>(vevent);
    long sum = 0, scan_sum = 0;
    (void)cm; (void)client_data;
    sum += event->integer_field % 100;
    sum += event->short_field % 100;
    sum += event->long_field % 100;
    sum += ((int) (event->nested_field.item.r * 100.0)) % 100;
    sum += ((int) (event->nested_field.item.i * 100.0)) % 100;
    sum += ((int) (event->double_field * 100.0)) % 100;
    sum += event->char_field;
    sum = sum % 100;
    scan_sum = event->scan_sum;
    if (sum != scan_sum) {
	printf("Received record checksum does not match. expected %d, got %d\n",
	       (int) sum, (int) scan_sum);
    }
    if ((quiet <= 0) || (sum != scan_sum)) {
	printf("In the handler, event data is :\n");
	printf("	integer_field = %d\n", event->integer_field);
	printf("	short_field = %d\n", event->short_field);
	printf("	long_field = %ld\n", event->long_field);
	printf("	double_field = %g\n", event->double_field);
	printf("	char_field = %c\n", event->char_field);
	printf("Data was received with attributes : \n");
	if (attrs) dump_attr_list(attrs);
    }
    message_count++;
    cout << "simple_handler message_count=" << message_count << "\n";
    return 0;
}

static int do_regression_master_test();

char *ECL_generate = "{\n\
    static int count = 0;\n\
    long sum = 0;\n\
    output.integer_field = (int) lrand48() % 100;\n\
    sum = sum + output.integer_field % 100;\n\
    output.short_field = ((short) lrand48());\n\
    sum = sum + output.short_field % 100;\n\
    output.long_field = ((long) lrand48());\n\
    sum = sum + output.long_field % 100;\n\
\n\
    output.nested_field.item.r = drand48();\n\
    sum = sum + ((int) (output.nested_field.item.r * 100.0)) % 100;\n\
    output.nested_field.item.i = drand48();\n\
    sum = sum + ((int) (output.nested_field.item.i * 100.0)) % 100;\n\
\n\
    output.double_field = drand48();\n\
    sum = sum + ((int) (output.double_field * 100.0)) % 100;\n\
    output.char_field = lrand48() % 128;\n\
    sum = sum + output.char_field;\n\
    sum = sum % 100;\n\
    output.scan_sum = (int) sum;\n\
    count++;\n\
    return count == 1;\n\
}";

char my_code[] = "{\n\
		output.r = 11.1;\n\
		output.i = 12.2;\n\
\n\
		return 1;\n\
}";

static int dont_fork = 0;
typedef struct {
    char *contact;
} alive_msg_t;
FMField alive_fields[] = {{"contact", "string", sizeof(char*), FMOffset(alive_msg_t*, contact)},
			  {NULL, NULL, 0, 0}};
FMStructDescRec alive_formats[]= {{"alive",alive_fields, sizeof(alive_msg_t), NULL}, {NULL,NULL,0,NULL}};


static char* argv0;

int
main(int argc, char **argv)
{
    int be_the_child = 0;

    argv0 = argv[0];
    while (argv[1] && (argv[1][0] == '-')) {
	if (argv[1][1] == 's') {
	    be_the_child = 1;
	} else if (argv[1][1] == 'q') {
	    quiet++;
	} else if (argv[1][1] == 'v') {
	    quiet--;
	} else if (argv[1][1] == 'n') {
	    dont_fork = 1;
	    quiet = -1;
	}
	argv++;
	argc--;
    }
    srand48(getpid());

    if (be_the_child) {
    	CManager cm = CManager_create();
    	attr_list parent_contact_list = attr_list_from_string(argv[1]);

        CMConnection conn = CMinitiate_conn(cm, parent_contact_list);
        alive_msg_t alive;
        CMFormat alive_format;
        attr_list tmp_list;
        alive.contact = attr_list_to_string(tmp_list = CMget_contact_list(cm));
        free_attr_list(tmp_list);
        alive_format = CMregister_format(cm, alive_formats);
        CMwrite(conn, alive_format, &alive);

    	/*    (void) CMfork_comm_thread(cm);*/
    	CMsleep(cm, 5);
    	CManager_close(cm);

    	return 0;
    } else {
    	return do_regression_master_test();
    }
}

static pid_t subproc_proc = 0;

static void
fail_and_die(int signal)
{
    (void)signal;
    fprintf(stderr, "EVtest failed to complete in reasonable time\n");
    if (subproc_proc != 0) {
    	kill(subproc_proc, 9);
    }
    exit(1);
}

static
pid_t
run_subprocess(char **args)
{
    if (dont_fork) {
    	int i = 0;
    	printf("Would have run :\n");
    	while(args[i] != NULL) {
    		printf("%s ", args[i++]);
    	}
    	printf("\n");
    	return 0;
    } else {
    	pid_t child;
    	if (quiet <=0) {
    		printf("Forking subprocess\n");
    	}
    	child = fork();
    	if (child == 0) {
    		/* I'm the child */
    		execv(args[0], args);
    	}
    	return child;
    }
}

static void
alive_handler(CManager cm, CMConnection conn, void *alive_v, 
	      void *client_data, attr_list attrs)
{
    EVstone stone, output_stone;
    EVaction action;
    EVstone local_stone;
    attr_list tmp_list;
    
    //char *action_spec = create_transform_action_spec(NULL, simple_format_list, ECL_generate);
    char *action_spec = create_transform_action_spec(NULL, complex_format_list,my_code);

 /*   (void) alive_v; (void) client_data; (void) attrs; */
    stone = REValloc_stone (conn);
    action = REVassoc_immediate_action (conn, stone, action_spec);
    output_stone = REValloc_stone (conn);
    REVaction_set_output(conn, stone, action, 0, output_stone);
    local_stone = EValloc_stone(cm);
    //EVassoc_terminal_action(cm, local_stone, simple_format_list, simple_handler, &message_count);
    EVassoc_terminal_action(cm, local_stone, complex_format_list, complex_handler, &message_count);
    
    REVassoc_bridge_action(conn, output_stone, tmp_list = CMget_contact_list(cm), local_stone);
    free_attr_list(tmp_list);
    free(action_spec);
    REVenable_auto_stone(conn, stone, 0, 100);
}

static int
do_regression_master_test()
{
    CManager cm;
    char *args[] = {argv0, "-s", NULL, NULL};
    int exit_state;
    int forked = 0, i;
    attr_list contact_list, listen_list = NULL;
    char *string_list, *transport, *postfix;
    CMFormat alive_format;

    cm = CManager_create();
    forked = CMfork_comm_thread(cm);
    CMlisten(cm);
    contact_list = CMget_contact_list(cm);
    if (contact_list) {
    	string_list = attr_list_to_string(contact_list);
    	free_attr_list(contact_list);
    }

    if (quiet <= 0) {
    	if (forked) {
    		printf("Forked a communication thread\n");
    	} else {
    		printf("Doing non-threaded communication handling\n");
    	}
    }
    srand48(1);

    args[2] = string_list;
    args[2] = static_cast<char*>(malloc(10 + strlen(string_list)));
    sprintf(args[2], "%s", string_list);
    alive_format = CMregister_format(cm, alive_formats);
    CMregister_handler(alive_format, alive_handler, NULL);
    subproc_proc = run_subprocess(args);

    /* give him time to start, run and send us data */
    for (i=0; i< 10; i++) {
    	if (message_count >= 1)
    		break;
    	CMsleep(cm, 1);
    }
    if (dont_fork) {
    	CMsleep(cm, 30);
    }

    free(string_list);
    free(args[2]);

    CManager_close(cm);

    cout << "message_count=" << message_count << "(1)\n";

    return !(message_count == 1);
}
