/**
 * triv_local.cpp
 *
 *  Created on: Dec 4, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */
#include <unistd.h>
#include <sys/types.h>

#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

#include "evpath.h"
#include "revpath.h"

#ifdef __cplusplus
}
#endif

#include "triv.h"
using namespace std;

// this convention is to get line numbers if something is wrong in COD
// '\n\'
char *ECL_generate = "{\n\
    static int count = 0;\n\
    long sum = 0;\n\
    output.integer_field = (int) lrand48() % 100;\n\
    sum = sum + output.integer_field % 100;\n\
    output.short_field = ((short) lrand48());\n\
    sum = sum + output.short_field % 100;\n\
    output.long_field = ((long) lrand48());\n\
    sum = sum + output.long_field % 100;\n\
\n\
    output.nested_field.item.r = drand48();\n\
    sum = sum + ((int) (output.nested_field.item.r * 100.0)) % 100;\n\
    output.nested_field.item.i = drand48();\n\
    sum = sum + ((int) (output.nested_field.item.i * 100.0)) % 100;\n\
\n\
    output.double_field = drand48();\n\
    sum = sum + ((int) (output.double_field * 100.0)) % 100;\n\
    output.char_field = lrand48() % 128;\n\
    sum = sum + output.char_field;\n\
    sum = sum % 100;\n\
    output.scan_sum = (int) sum;\n\
    count++;\n\
    return count == 1;\n\
}";

char *my_code="{\n\
		output.r = 13.0;\n\
		output.i = 13.13;\n\
		\n\
		return 1;\n\
	}";



static int complex_handler(CManager cm, void *vevent, void *client_data, attr_list attrs){
	struct complex_rec *event = static_cast<struct complex_rec *> (vevent);

	std::cout << "Complex=( " << event->r << "," << event->i << " )\n";

	return 0;

}

static int message_count = 0;

static void alive_handler(CManager cm, CMConnection conn, void *alive_v, void *client_data, attr_list attrs){
	EVstone rstone;        // remote stone
	EVstone routput_stone; // remote output stone
	EVaction action;
	EVstone local_stone;
	attr_list tmp_list;

	cout << "Received alive message\n" ;

	// do remotely evpath actions
	// first is incoming event data that the transformation will produce
	// a description of the outgoing event data
	char *action_spec = create_transform_action_spec(nullptr, complex_format_list, my_code);

	// allocate a remote stone
	rstone = REValloc_stone(conn);
	action = REVassoc_immediate_action (conn, rstone, action_spec);
	free(action_spec);

	// create the bridge stone remotely
	routput_stone = REValloc_stone(conn);
	REVaction_set_output(conn, rstone, action, 0, routput_stone);

	local_stone = EValloc_stone(cm);
	EVassoc_terminal_action(cm, local_stone, complex_format_list, complex_handler, &message_count);

	// connect the remote stone to the newly created local stone
	REVassoc_bridge_action(conn, routput_stone, tmp_list = CMget_contact_list(cm),
			local_stone);

	free_attr_list(tmp_list);
	cout << "Enabling remote auto stone ....\n";

	// 0 - period_sec the period at which submits should occur
	// 10 - period_usec the period at which submits should occur, microseconds portion
	REVenable_auto_stone(conn, rstone, 0, 10);

}


int main(int argc, char **argv){

	CManager cm;
	attr_list contact_list, listen_list = nullptr;

	// create a connection
	cm = CManager_create();

	// I guess this is an internal thread that will handle the network
	// and there is no need to call CMrun_network since this created
	// thread will take care of that
	// TODO don't know how can I connect to this thread
	CMfork_comm_thread(cm);

	// TODO CMlisten_specific
	// Tell CM to listen for incoming network connections with
	// specific characteristics. I think I can skip it because
	// I have nothing specific
	//CMlisten_specific(cm, listen_list);
	CMlisten(cm);

	contact_list = CMget_contact_list(cm);

	char *string_list = nullptr;
	if (contact_list) {
		string_list = attr_list_to_string(contact_list);
		free_attr_list(contact_list);
	} else {
		std::cerr << "ERROR: Issues with getting the contact list. Quitting ...\n";
		return -1;
	}

	cout << "Forked a communication thread\n";
	CMFormat alive_format;
	alive_format = CMregister_format(cm, alive_format_list);

	CMregister_handler(alive_format, alive_handler, nullptr);

	std::cout << "Run remote with: " << string_list << "\n";

	// delay
	for (int i = 0; i < 10; ++i){
		cout << "i=" << i << "," << "message count=" << message_count << "\n";
		if (1 == message_count ){
			break;
		}
		CMsleep(cm, 1);
	}
	// use the main program's thread of control to service incoming messages
	//CMrun_network(cm);

	CMsleep(cm, 120);

	free(string_list);
	CManager_close(cm);
	cout << "Message count = " << message_count << "\n";

	return !(1 == message_count );

	return 0;
}
