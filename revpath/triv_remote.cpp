/**
 * triv_remote.cpp
 *
 *  Created on: Dec 4, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */

#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

#include "evpath.h"
#include "revpath.h"

#ifdef __cplusplus
}
#endif

#include "triv.h"

using namespace std;

int main(int argc, char **argv){

	if (1 == argc) {
		cout << "Usage: " << argv[0] << " contact-list\n";
		cout << "contact-list this is what printed local\n";
		cout << "E.g., " << argv[0] << " AAIAAJTJ8o2qZQAAATkCmEQ0PYA=\n";

		return 0;
	}
	CManager cm = CManager_create();


	attr_list local_contact_list = attr_list_from_string(argv[1]);

	CMConnection conn = CMinitiate_conn(cm, local_contact_list);

	if (nullptr == conn){
		cout << "No connection, attr list was: ";
		dump_attr_list(local_contact_list);
		cout << "\n";
		return 0;
	}

	struct alive_msg alive;
	CMFormat alive_format;
	attr_list tmp_list;
	alive.contact = attr_list_to_string(tmp_list = CMget_contact_list(cm));
	free_attr_list(tmp_list);
	alive_format = CMregister_format(cm, alive_format_list);
	cout << "Sending a contact\n";
	CMwrite(conn, alive_format, &alive);

	// service network for 600 seconds
	CMsleep(cm, 600);
	CManager_close(cm);

	return 0;
}
