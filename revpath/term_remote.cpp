/**
 * term_remote.cpp
 *
 *  Created on: Dec 9, 2013
 *  Author: Magda Slawinska aka Magic Magg magg dot gatech at gmail.com
 */


#include <unistd.h>
#include <sys/types.h>
#include <iostream>

#ifdef __cplusplus
extern "C" {
#endif

#include "evpath.h"
#include "revpath.h"

#ifdef __cplusplus
}
#endif

#include "triv.h"


using namespace std;


int main(int argc, char **argv){
	if( argc != 2 ){
		cout << "Usage: " << argv[0] << " contact-string\n";
		cout << "Eg. " << argv[0] << "AAIAAJTJ8o2yZQAAATkCmEoBqMA=\n";
		return 0;
	}

	CManager cm = CManager_create();
	attr_list parent_contact_list = attr_list_from_string(argv[1]);

	// initiate the connection with the local
    CMConnection conn = CMinitiate_conn(cm, parent_contact_list);


    // register the format that you will send
    CMFormat alive_format;
    alive_format = CMregister_format(cm, alive_format_list);

    attr_list tmp_list = CMget_contact_list(cm);
    struct alive_msg alive;
    alive.contact = attr_list_to_string(tmp_list);
    free_attr_list(tmp_list);

    // send the contact info to the local
    CMwrite(conn, alive_format, &alive);

	/*    (void) CMfork_comm_thread(cm);*/
	CMsleep(cm, 1);
	CManager_close(cm);

	cout << "Done!\n";

	return 0;
}
