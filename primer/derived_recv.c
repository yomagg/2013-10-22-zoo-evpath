#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "evpath.h"

typedef struct _simple_rec {
    int integer_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer", sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};

static int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_rec_ptr event = vevent;
    printf("I got %d\n", event->integer_field);
    return 1;
}

/* this file is evpath/examples/derived_recv.c */
int main(int argc, char **argv)
{
    CManager cm;
    EVstone stone;
    char *string_list, *filter_spec, *encoded_filter_spec;

    cm = CManager_create();
    CMlisten(cm);

    stone = EValloc_stone(cm);
    EVassoc_terminal_action(cm, stone, simple_format_list, simple_handler, NULL);

    string_list = attr_list_to_string(CMget_contact_list(cm));

    // encode the receiver filter specification, i.e., encode
    // the data type that the filter expects and the body of the function
    // if the function returns false (0), the event will be discarded
    // for non-zero returns the event will be passed unchanged through
    // the filter
    filter_spec = create_filter_action_spec(simple_format_list, 
					    "{ return input.integer_field % 2;}");
    // encode the receiver filter specification; make it shell safe
    encoded_filter_spec = atl_base64_encode(filter_spec, strlen(filter_spec) + 1);

    // add the filter specification to the contact list
    printf("Contact list \"%d:%s:%s\"\n", stone, string_list, encoded_filter_spec);
    free(filter_spec);
    free(encoded_filter_spec);

    // instead of CMrun_network -> CMsleep; service the network for
    // 600 seconds and then exit.
    CMsleep(cm, 600);
}
