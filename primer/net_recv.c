#include <stdio.h>
#include <string.h>
#include "evpath.h"

typedef struct _simple_rec {
    int integer_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer", sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};

static int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_rec_ptr event = vevent;
    printf("I got %d\n", event->integer_field);
    return 1;
}

/* this file is evpath/examples/net_recv.c */
int main(int argc, char **argv)
{
    CManager cm;
    EVstone stone;
    char *string_list;

    // create a connection
    cm = CManager_create();
    CMlisten(cm);

    // create a stone
    stone = EValloc_stone(cm);
    // associate a terminal action with the stone and data
    EVassoc_terminal_action(cm, stone, simple_format_list, simple_handler, NULL);

    // extract contact information from CM so it can be provided to the sender
    string_list = attr_list_to_string(CMget_contact_list(cm));
    // print the stone id and the stringified contact information is printed
    printf("Contact list (stone id:contact info) \"%d:%s\"\n", stone, string_list);

    // use the main program's thread of control to service incoming messages
    CMrun_network(cm);
}
