#ifdef __cplusplus
extern "C" {
#endif
/* this file is evpath/examples/triv.c */
#include "evpath.h"
#ifdef __cplusplus
} // closing brace for extern "C"
#endif

#include <iostream>

// all data are typed and types play the role in determining actions
typedef struct _simple_rec {
    int integer_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer", sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};

// consumer of events
static int
simple_handler(CManager cm, void *vevent, void *client_data, attr_list attrs)
{
    simple_rec_ptr event = static_cast<simple_rec_ptr> (vevent);
    std::cout << "I got " << event->integer_field << "\n";
    return 1;
}

int main(int argc, char **argv)
{
    CManager cm;
    simple_rec data;
    EVstone stone;
    EVsource source;

    // create a cm manager to handle network; evpath is network focused
    cm = CManager_create();

    // create a source stone
    stone = EValloc_stone(cm);
    // associate the terminal action with the stone and a description of
    // the data that will be handled by this stone
    EVassoc_terminal_action(cm, stone, simple_format_list, simple_handler, NULL);

    source = EVcreate_submit_handle(cm, stone, simple_format_list);
    data.integer_field = 217;
    EVsubmit(source, &data, NULL);
}
