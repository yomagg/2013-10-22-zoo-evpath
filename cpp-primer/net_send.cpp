// $ ./net_recv
// Contact list "0:AAIAAJTJ8o3DZQAAATkCmFHN148="
// $ ./net_send
// $ ./net_send 0:AAIAAJTJ8o3DZQAAATkCmFHN148=


#ifdef __cplusplus
extern "C" {
#endif
#include "evpath.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef __cplusplus
}
#endif

#include <iostream>

typedef struct _simple_rec {
    int integer_field;
} simple_rec, *simple_rec_ptr;

static FMField simple_field_list[] =
{
    {"integer_field", "integer", sizeof(int), FMOffset(simple_rec_ptr, integer_field)},
    {NULL, NULL, 0, 0}
};
static FMStructDescRec simple_format_list[] =
{
    {"simple", simple_field_list, sizeof(simple_rec), NULL},
    {NULL, NULL}
};

/* this file is evpath/examples/net_send.c */
int main(int argc, char **argv)
{
    CManager cm;
    simple_rec data;
    EVstone stone;
    EVsource source;
    char string_list[2048];
    attr_list contact_list;
    EVstone remote_stone;

    // extract contact information: the remote (receiver) stone id
    // and a contact information
    if (2 < argc ){
    	std::cout << "Bad arguments: eg. " << argv[0] << " 0:AAIAAJTJ8o2jZQAAATkCmEoBqMA=" << "\n";
    	return -1;
    }
    if (sscanf(argv[1], "%d:%s", &remote_stone, &string_list[0]) != 2) {
    	std::cout << "Bad arguments \"" << argv[1] << "\"\n";
    	exit(0);
    }

    cm = CManager_create();
    CMlisten(cm);

    stone = EValloc_stone(cm);
    // use the contact list to connect to the receiver
    contact_list = attr_list_from_string(string_list);
    // remote_stone will hold the identifier of the receiver stone
    // you need to unstringify the contact list; bridge stone
    // sends the data to the receiver
    EVassoc_bridge_action(cm, stone, contact_list, remote_stone);

    source = EVcreate_submit_handle(cm, stone, simple_format_list);
    data.integer_field = 318;
    EVsubmit(source, &data, NULL);
}
