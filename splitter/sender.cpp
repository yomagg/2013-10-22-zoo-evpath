/**
 * local.cpp
 *
 *  Created on: Dec 10, 2013
 *  Author: Magdalena Slawinska aka Magic Magg, magg dot gatech at gmail.com
 */

#include <iostream>
#include <unistd.h>
#include <vector>
#include <sstream>

#include "evpath.h"


#include "data.h"

using namespace std;

int main(int argc, char * argv[]){

	if (argc < 2){
		cout << "USAGE\n\n";
		cout << argv[0] << " how-long-to-run remote-stone-id1:contact-string remote-stone-id2:contact-string ...\n";
		cout << "  how-long-to-run tells how long the " << argv[0] << " will run in seconds before removing of stones begins\n";
		cout << "  remote-stone-id1:contact-string contact information output by the receiver 1\n";
		cout << "  remote-stone-id2:contact-string contact information output by the receiver 2\n";
		cout << "\nDESCRIPTION\n\n";
		cout << "The program creates the splitter stone and as many output stones as remote stones ids";
		cout << " provided as input parameters. It sends events for a period of time";
		cout << " and starts removing output stones periodically.\n";
		cout << "\nEXAMPLE\n\n" << argv[0] << " 600 0:AAIAAJTJ8o2yZQAAATkCmEoBqMA= 0:AAIAAJTJ8u2yZQAAATkCmEoBqMA=\n";
		return 0;
	}

	CManager cm;

	cm = CManager_create();
	CMfork_comm_thread(cm);
	CMlisten(cm);

	// create a split stone - allocate a stone and associate a split
	// action with the stone
	EVstone split_stone = EValloc_stone(cm);
	EVaction split_action = EVassoc_split_action(cm, split_stone, nullptr);

	// get a contact list
	attr_list contact_list = nullptr;
	contact_list =  CMget_contact_list(cm);

	char *contact_list_str=nullptr;
	if (contact_list) {
		contact_list_str = attr_list_to_string(contact_list);
		free_attr_list(contact_list);
		cout << "My contact split_stone_id:split_action_id:contact: "
			 << split_stone << ":" << split_action << ":"
			 << contact_list_str << "\n";
	} else {
		cerr << "ERROR: Issues with getting the contact list. Quitting ...\n";
		return 1;
	}

	vector<EVstone> output_stone_ids;

	for(int i = 2; i < argc; ++i){
		string contact_info = argv[i];
		stringstream strm(contact_info);
		EVstone remote_stone_id;
		char delim;
		strm >> remote_stone_id >> delim;
		stringstream remote_stone_contact;

		strm.get(* remote_stone_contact.rdbuf());

		// create a local output stone and connect this stone with the
		// remote stone
		EVstone output_stone_id = EValloc_stone(cm);
		contact_list = attr_list_from_string(remote_stone_contact.str().c_str());

		// remember the output stone id
		output_stone_ids.push_back(output_stone_id);

		EVassoc_bridge_action(cm, output_stone_id, contact_list, remote_stone_id);
		// the below call adds the output stone to the list of stones
		// to which the split stone will replicate data. The call requires
		// the stone to which the split action was registered and the
		// split action that was returned
		EVaction_add_split_target(cm, split_stone, split_action, output_stone_id);

		cout << "Output stone id: " << output_stone_id << ", created and added as a split target\n";
		free_attr_list(contact_list);
	}

	EVsource source = EVcreate_submit_handle(cm, split_stone, complex_format_list);

	struct complex_rec data;
	data.r = 13.3;
	data.i = 12.3;

	cout << "Sending ...\n";

	for(int i = 1; i < atoi(argv[1]); ++i){
		cout << "* ";
		// The order EVsubmit,CMsleep or CMsleep,EVsubmit is somewhat important
		// in CMsleep,EVsubmit the last message will probably be not delivered
		EVsubmit(source, &data, nullptr);
		CMsleep(cm, 1);
		data.r++;
		data.i++;
	}

	cout << "\nStarting removal of split targets ...\n";

	// remove one output stone
	for( auto id : output_stone_ids){

		EVaction_remove_split_target(cm, split_stone, split_action,id);
		cout << "\nSplit target id: " << id << " removed\n";


		// <<<< TODO  this routine causes the sender to hang up
	    if( 1 == EVdestroy_stone(cm, id)){
			cout << "Output_stone_id: " << id << " destroyed\n";
		} else  {
			cout << "ERROR: issues with destroying a stone. Continueing ...\n";
		}
		// TODO >>>>


		for(int i = 1; i < 10; ++i){
			cout << "* ";
			// The order EVsubmit,CMsleep or CMsleep,EVsubmit is somewhat important
			// in CMsleep,EVsubmit the last message will probably be not delivered
			EVsubmit(source, &data, nullptr);
			CMsleep(cm, 1);
			data.r++;
			data.i++;
		}
	}

	cout << "\n";
	CManager_close(cm);

	return 0;
}

